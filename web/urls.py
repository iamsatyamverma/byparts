from django.conf.urls import url

from web.views import AppView

urlpatterns = [
    # TODO: Replace the ui prefix to serve on /
    url(r'^ui/', AppView.as_view()),
    url(r'^(?:.*)/?$', AppView.as_view()),  # Added to handle react router
]
