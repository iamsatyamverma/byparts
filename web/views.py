from django.conf import settings
from django.shortcuts import render
from django.views import View


class AppView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'index.html', {'static_url': settings.UI_BUNDLE_URL})

