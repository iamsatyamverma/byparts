from datetime import datetime
from django.utils import timezone
from django.conf import settings


def parse_datetime(date_time, dtformat=None):

    if date_time is None:
        return None

    if dtformat is None:
        if len(date_time) == 10:
            dt_parsed = datetime.strptime(date_time, settings.DATE_FORMAT)
        elif len(date_time) == 20:
            dt_parsed = datetime.strptime(date_time, settings.DATE_TIME_FORMAT)
        elif len(date_time) == 24:
            dt_parsed = datetime.strptime(date_time, settings.DATE_TIME_MILLIS_FORMAT)
        else:
            raise ValueError("Cannot parse date time %s" % date_time)
    else:
        dt_parsed = datetime.strptime(date_time, dtformat)

    tz = timezone().get_current_timezone()

    return timezone.make_aware(dt_parsed, tz)
