import os

from django.contrib.contenttypes.models import ContentType
from django.core.files.storage import default_storage

from core.models import CustomImageAttribute, TemporaryFiles

PART_IMAGE_CONTENT_TYPE = ContentType.objects.get(app_label='ecommerce', model='inventoryrequest')

def delete_file_from_disk(filepath):
    default_storage.delete(filepath)

def create_image_attribute(temp_id, object_id, custom_attribute_type, path, content_type):
    temporary_image_obj = TemporaryFiles.objects.get(id=temp_id)

    image_attribute = CustomImageAttribute(
                        object_id=object_id,
                        type=custom_attribute_type,
                        _value=path,
                        content_type=content_type
                        )

    file_name = temporary_image_obj.name

    if (file_name == ""):
        temp_path, file_name = os.path.split(temporary_image_obj.file.name)

    path = '{}/{}'.format(path, file_name)

    image_attribute.save(temporary_image_obj.file, path)

    image_path = image_attribute.image.name

    return {'id':image_attribute.id, 'path':image_path}
