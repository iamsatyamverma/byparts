
from django.conf.urls import url, include

from django.contrib.auth.models import User
from knox import views as knox_views
from rest_framework import routers, serializers, viewsets

from core.views import LoginView, Register, OrganizationStoreView, TemporaryFilesView, UserMetaView


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'is_staff')

# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'profile', UserMetaView)
router.register(r'store_location', OrganizationStoreView)
router.register(r'temporary_file', TemporaryFilesView)


urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'auth/register', Register.as_view(), name='register'),
    url(r'auth/login', LoginView.as_view(), name='knox_login'),
    url(r'auth/logout/', knox_views.LogoutView.as_view(), name='knox_logout'),
    url(r'auth/logoutall/', knox_views.LogoutAllView.as_view(), name='knox_logoutall'),
    ]
