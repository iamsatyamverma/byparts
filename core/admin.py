from django.contrib import admin

from core.models import Role, BusinessType, Organization, UserProfile, CustomAttributeType, CustomAttributeBase, \
    CustomImageAttribute, VehicleType, Shape, Make, Model, Part, PartCategory, PartType, CustomUserAttribute, \
    OrganizationStore, TemporaryFiles, UserMeta

admin.site.register(Role)
admin.site.register(BusinessType)
admin.site.register(Organization)
admin.site.register(UserProfile)
admin.site.register(UserMeta)

admin.site.register(CustomAttributeType)
admin.site.register(CustomImageAttribute)
admin.site.register(VehicleType)
admin.site.register(Shape)
admin.site.register(Make)
admin.site.register(Model)
admin.site.register(Part)
admin.site.register(PartCategory)
admin.site.register(PartType)
admin.site.register(CustomUserAttribute)
admin.site.register(OrganizationStore)
admin.site.register(TemporaryFiles)

