import os
import uuid

from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from core.models import UserProfile, Organization, VehicleType, Shape, Make, Model, OrganizationStore, TemporaryFiles, \
    CustomImageAttribute, CustomAttributeType, UserMeta


class UserDetailsSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
            required=True,
            validators=[UniqueValidator(queryset=User.objects.all())]
            )

    username = serializers.CharField(
            validators=[UniqueValidator(queryset=User.objects.all())]
            )

    password = serializers.CharField(min_length=8)

    def create(self, validated_data):
        user = User.objects.create_user(validated_data['username'],
                                        validated_data['email'],
                                        validated_data['password'])
        return user

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password')


class UserProfileSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserProfile
        fields = ('id', 'organization', 'owner', 'image_url', 'phone', 'fax', 'role')

class UserMetaSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserMeta
        fields = '__all__'

'''
    REPAIRER_TYPES = [('Collision Repairer', 'Collision Repairer'), ('Mechanical Repairer', 'Mechanical Repairer')]

    is_supplier = fields.BooleanField()
    name = fields.CharField(max_length=248)
    reference = fields.CharField(max_length=248)
    uen = fields.CharField(max_length=248)
    primary_business = models.ManyToManyField(BusinessType)
    repairer_type = fields.CharField(max_length=248, choices=REPAIRER_TYPES)
    email = fields.CharField(max_length=248)
    phone = fields.CharField(max_length=31, null=True, blank=True)
    fax = fields.CharField(max_length=31, null=True, blank=True)
    address = fields.TextField()
    postal_address = fields.TextField()
    company_url = models.URLField()
    description = fields.CharField(max_length=248, blank=True, null=True) #organisation description
'''
'''
    organisation = models.ForeignKey(Organisations)
    owner = models.OneToOneField(User,related_name="profile")
    image_url = fields.URLField(null=True, blank=True)
    phone = fields.CharField(max_length=31, null=True, blank=True)
    fax = fields.CharField(max_length=31, null=True, blank=True)
    role = models.ManyToManyField(Role)
'''

class OrganizationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Organization
        fields = ('id', 'is_supplier', 'name', 'reference',
                  'uen', 'primary_business', 'email', 'phone',
                  'fax', 'address', 'postal_address', 'company_url',
                  'description')


class VehicleTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = VehicleType
        fields = '__all__'


class ShapeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Shape
        fields = '__all__'


class MakeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Make
        fields = '__all__'


class ModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = Model
        fields = '__all__'


class PartCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Model
        fields = '__all__'


class PartTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Model
        fields = '__all__'


class PartSerializer(serializers.ModelSerializer):
    class Meta:
        model = Model
        fields = '__all__'


class OrganizationStoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrganizationStore
        fields = '__all__'


class TemporaryFileSerializer(serializers.ModelSerializer):
    name = serializers.CharField()
    file = serializers.FileField(max_length=100000,
                                 allow_empty_file=False,
                                 use_url=False )

    class Meta:
        model = TemporaryFiles
        fields = ('id', 'file', 'name')


class CustomImageAttributeSerializer(serializers.Serializer):
    custom_attribute_type = serializers.PrimaryKeyRelatedField(queryset=CustomAttributeType.objects.all())
    temporary_image = serializers.PrimaryKeyRelatedField(queryset=TemporaryFiles.objects.all())
    content_type = serializers.PrimaryKeyRelatedField(queryset=ContentType.objects.all())
    object_id = serializers.IntegerField()
    path = serializers.CharField()

    def create(self, validated_data):
        temporary_image_obj = validated_data['temporary_image']

        image_attribute = CustomImageAttribute(
            object_id=validated_data['object_id'],
            type=validated_data['custom_attribute_type'],
            _value=validated_data['path'],
            content_type=validated_data['content_type']
        )

        file_name = temporary_image_obj.name

        if (file_name == ""):
            temp_path, file_name = os.path.split(temporary_image_obj.file.name)

        path = '{}/{}'.format(validated_data['path'], file_name)

        image_attribute.save(temporary_image_obj.file, path)

        return image_attribute

    def to_representation(self, instance):

        return instance.image.name