from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from knox.views import LoginView as KnoxLoginView
from rest_framework import viewsets
from rest_framework.authentication import BasicAuthentication
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST
from core.models import Role, BusinessType, VehicleType, Shape, Make, Model, PartCategory, PartType, Part, \
    OrganizationStore, TemporaryFiles, UserMeta
from core.serializers import UserDetailsSerializer, UserProfileSerializer, UserMetaSerializer, OrganizationSerializer, VehicleTypeSerializer, \
    ShapeSerializer, MakeSerializer, ModelSerializer, PartCategorySerializer, PartTypeSerializer, PartSerializer, \
    OrganizationStoreSerializer, TemporaryFileSerializer


class LoginView(KnoxLoginView):
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        username = request.data.get('username', '')
        password = request.data.get('password', '')

        user = User.objects.filter(username=username)

        if user.exists():
            user_obj = user[0]
        else:
            user = User.objects.filter(email=username)
            if user.exists():
                user_obj = user[0]
            else:
                raise ValidationError({"details": "User does not exists"})

        if not user_obj.is_active:
            raise ValidationError({
                                    "details": " Subscription Has Expired. Please contact admin for related queries."})

        username = user_obj.username

        user = authenticate(username=username, password=password)

        if user is None:
            raise ValidationError({"details": "User/ Password does not match"})

        request.user = user_obj

        return super(LoginView, self).post(request, *args, **kwargs)


class Register(KnoxLoginView):
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        response_data = {}
        profile_data = {}

        profile_data['phone'] = request.data.pop('phone', None)
        profile_data['company'] = request.data.pop('company', None)
        profile_data['job_title'] = request.data.pop('job_title', None)
        profile_data['country'] = request.data.pop('country', None)
        profile_data['postal_code'] = request.data.pop('postal_code', None)
        profile_data['revenue'] = request.data.pop('revenue', None)
        profile_data['company_size'] = request.data.pop('company_size', None)
        profile_data['first_name'] = request.data.pop('first_name', None)
        profile_data['last_name'] = request.data.pop('last_name', None)

        user_serializer = UserDetailsSerializer(data=request.data)

        if user_serializer.is_valid(raise_exception=True):

            user = user_serializer.save()

            response_data.update(user_serializer.data)



            profile_data['owner'] = user.id

            if user:
                profile_serializer = UserMetaSerializer(
                                                        data=profile_data,
                                                        context={'request': request}
                                                        )

                if profile_serializer.is_valid():
                    profile_serializer.save()
                    response_data.update(profile_serializer.data)

                else:
                    user.delete()

                    return Response(profile_serializer.errors, status=HTTP_400_BAD_REQUEST)

        return Response(response_data)


class UserMetaView(viewsets.ModelViewSet):
    queryset = UserMeta.objects.all()
    serializer_class = UserMetaSerializer
    # permission_classes = (IsAuthenticated)


class TemporaryFilesView(viewsets.ModelViewSet):
    queryset = TemporaryFiles.objects.all()
    serializer_class = TemporaryFileSerializer
    permission_classes = (IsAuthenticated,)


class VehicleTypeView(viewsets.ModelViewSet):
    queryset = VehicleType.objects.all()
    serializer_class = VehicleTypeSerializer
    permission_classes = (IsAuthenticated,)


class ShapeView(viewsets.ModelViewSet):
    queryset = Shape.objects.all()
    serializer_class = ShapeSerializer
    permission_classes = (IsAuthenticated,)


class MakeView(viewsets.ModelViewSet):
    queryset = Make.objects.all()
    serializer_class = MakeSerializer
    permission_classes = (IsAuthenticated,)


class ModelView(viewsets.ModelViewSet):
    queryset = Model.objects.all()
    serializer_class = ModelSerializer
    permission_classes = (IsAuthenticated,)


class PartCategoryView(viewsets.ModelViewSet):
    queryset = PartCategory.objects.all()
    serializer_class = PartCategorySerializer
    permission_classes = (IsAuthenticated,)


class PartCategoryView(viewsets.ModelViewSet):
    queryset = PartCategory.objects.all()
    serializer_class = PartCategorySerializer
    permission_classes = (IsAuthenticated,)


class PartTypeView(viewsets.ModelViewSet):
    queryset = PartType.objects.all()
    serializer_class = PartTypeSerializer
    permission_classes = (IsAuthenticated,)


class PartView(viewsets.ModelViewSet):
    queryset = Part.objects.all()
    serializer_class = PartSerializer
    permission_classes = (IsAuthenticated,)


class OrganizationStoreView(viewsets.ModelViewSet):
    queryset = OrganizationStore.objects.all()
    serializer_class = OrganizationStoreSerializer
    permission_classes = (IsAuthenticated,)
