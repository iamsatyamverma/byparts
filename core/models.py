from io import StringIO
import os
from PIL import Image as PilImage
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ObjectDoesNotExist
from django.core.files.base import ContentFile
from django.core.files.storage import Storage, default_storage
from django.db import models
from django.db.models import fields
from django.db.models.signals import pre_delete
from django.dispatch.dispatcher import receiver
from django.utils import timezone
from rest_framework.exceptions import ValidationError

from core import utils


class ModelBase(models.Model):
    created_at = fields.DateTimeField(editable=False)
    last_updated_at = fields.DateTimeField(editable=False)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if not self.id:
            self.created_at = timezone.now()

        self.last_updated_at = timezone.now()
        super(ModelBase, self).save(force_insert, force_update, using,
                                    update_fields)

    class Meta:
        abstract = True


class Role(ModelBase):
    title = fields.CharField(max_length=128, unique=True)
    description = fields.TextField()

    def __unicode__(self):
        return "{}".format(self.title)


class BusinessType(ModelBase):
    name = fields.CharField(max_length=248)
    description = fields.CharField(max_length=248, blank=True, null=True)


class Organization(ModelBase):
    is_supplier = fields.BooleanField()
    name = fields.CharField(max_length=248)
    reference = fields.CharField(max_length=248)
    uen = fields.CharField(max_length=248)
    primary_business = models.ManyToManyField(BusinessType)
    email = fields.CharField(max_length=248)
    phone = fields.CharField(max_length=31, null=True, blank=True)
    fax = fields.CharField(max_length=31, null=True, blank=True)
    address = fields.TextField()
    postal_address = fields.TextField()
    company_url = models.URLField()
    description = fields.CharField(max_length=248, blank=True, null=True) #organisation description

    class Meta:
        ordering = ('name',)

    def __unicode__(self):
        return self.name


class OrganizationStore(ModelBase):
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    name = fields.CharField(max_length=255, unique=True)
    location = fields.TextField()


class UserProfile(ModelBase):
    organization = models.ForeignKey(Organization, related_name="user_profiles", on_delete=models.CASCADE)
    owner = models.OneToOneField(User, related_name="profile", on_delete=models.CASCADE)
    image_url = fields.URLField(null=True, blank=True)
    phone = fields.CharField(max_length=31, null=True, blank=True)
    fax = fields.CharField(max_length=31, null=True, blank=True)
    role = models.ManyToManyField(Role)

    def __unicode__(self):
        return "{}".format(self.owner.username)


class UserMeta(ModelBase):
    owner = models.OneToOneField(User, related_name="meta", on_delete=models.CASCADE)
    phone = fields.CharField(max_length=31, null=True, blank=True)
    company = fields.TextField(null=True, blank=True)
    job_title = fields.CharField(max_length=31, null=True, blank=True)
    country = fields.CharField(max_length=31, null=True, blank=True)
    region = fields.CharField(max_length=31, null=True, blank=True)
    postal_code = fields.CharField(max_length=31, null=True, blank=True)
    industry = fields.CharField(max_length=31, null=True, blank=True)
    revenue = fields.CharField(max_length=31, null=True, blank=True)
    company_size = fields.CharField(max_length=31, null=True, blank=True)
    first_name = fields.CharField(max_length=31, null=True, blank=True)
    last_name = fields.CharField(max_length=31, null=True, blank=True)


    def __unicode__(self):
        return "{}".format(self.owner.username)


DATATYPE_LIST = [('int', 'int'), ('str', 'str'),
                 ('singleselect', 'singleselect'), ('date', 'date'),
                 ('float', 'float'), ('bool', 'bool'), ('url', 'url'),
                 ('longstr', 'longstr')]

class CustomAttributeType(ModelBase):
    name = models.SlugField()
    value_type = models.CharField(max_length=31, choices=DATATYPE_LIST,
                                  default="str")
    target_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)

    def __unicode__(self):
        return "{}".format(self.name)

    def value(self, text_value):
        if self.value_type == 'int':
            return int(text_value)
        elif self.value_type == 'float':
            return float(text_value)
        elif self.value_type == 'singleselect':
            return str(text_value)
        elif self.value_type == 'date':
            return utils.parse_datetime(text_value).date()
        else:
            return text_value


class CustomAttributeBase(ModelBase):
    type = models.ForeignKey(CustomAttributeType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField(null=True, blank=True)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    target = GenericForeignKey('content_type', 'object_id')
    notes = fields.CharField(max_length=255, null=True, blank=True)
    is_active = models.BooleanField(default=True)

    _value = models.TextField(blank=True)

    class Meta:
        abstract = True
        index_together = [["type", "object_id", "last_updated_at"], ]

    def __unicode__(self):
        return "{}:{}".format(self.target, self.type.name)

    def get_value(self):
        return self.type.value(self._value)

    def set_value(self, value):
        self._value = self.type.convert_to_str(value)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        ModelBase.save(self, force_insert=force_insert,
                       force_update=force_update, using=using,
                       update_fields=update_fields)

    value = property(get_value, set_value)


def get_picture_path(instance, filename):
    return filename

def get_thumbnail_path(instance, complete_path):
    directoryname, filename = os.path.split(complete_path)
    parentdirname, raw = os.path.split(directoryname)
    complete_path = os.path.join(parentdirname, "thumb", filename)
    return complete_path


class CustomImageAttribute(CustomAttributeBase):
    image = models.ImageField(upload_to=get_picture_path, max_length=255)
    thumbnail = models.ImageField(upload_to=get_thumbnail_path, max_length=253,
                                  null=False, default='thumb.jpg')

    def get_extension(self):
        name, extension = os.path.splitext(self.image.name)
        return extension

    def save(self, *args, **kwargs):
        if args:
            image_data, image_path = args
            self.image.save(image_path, image_data)

            # storage_image = Storage.open(self.image.storage,self.image.name,'r')
            # original_image = PilImage.open(storage_image)
            # original_image.thumbnail(settings.THUMBNAIL_SIZE, PilImage.ANTIALIAS)
            # filepointer = StringIO.StringIO()
            # original_image.save(filepointer, original_image.format, quality=95)
            # contentfile = ContentFile(filepointer.getvalue())
            # self.thumbnail.save(name=self.image.name, content=contentfile, save=False)

            super(CustomImageAttribute, self).save()

    def __unicode__(self):
        return self.image.name


def get_temporary_path(instance, filename):
    return '{}/{}'.format('temp', filename)


class TemporaryFiles(ModelBase):
    name = models.CharField(max_length=128)
    file = models.FileField(max_length=255, upload_to=get_temporary_path)

    def __unicode__(self):
        return self.file.name


class CustomUserAttribute(CustomAttributeBase):
    user = models.ForeignKey(User, on_delete=models.CASCADE)


class VehicleType(ModelBase):
    name = fields.CharField(max_length=255, unique=True)


class Shape(ModelBase):
    vehicle_type = models.ForeignKey(VehicleType, on_delete=models.CASCADE)
    name = fields.CharField(max_length=255, unique=True)


class Make(ModelBase):
    vehicle_type = models.ForeignKey(VehicleType, on_delete=models.CASCADE)
    name = fields.CharField(max_length=255, unique=True)

    class Meta:
        unique_together = ('vehicle_type', 'name')

class Model(ModelBase):
    make = models.ForeignKey(Make, on_delete=models.CASCADE)
    shape = models.ForeignKey(Shape, on_delete=models.CASCADE)
    name = fields.CharField(max_length=255, unique=True)

    class Meta:
        unique_together = ('make', 'shape', 'name')


class PartCategory(ModelBase):
    name = fields.CharField(max_length=255)


class PartType(ModelBase):
    category = models.ForeignKey(Model, on_delete=models.CASCADE)
    name = fields.CharField(max_length=255)


class Part(ModelBase):
    category = models.ForeignKey(PartType, on_delete=models.CASCADE)
    name = fields.CharField(max_length=255)

    class Meta:
        unique_together = ('category', 'name')

@receiver(pre_delete, sender=CustomImageAttribute)
def delete_image(sender, instance, *args, **kwargs):
    default_storage.delete(instance.image.name)
