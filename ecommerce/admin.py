from django.contrib import admin

from ecommerce.models import InventoryRequest, InventoryQuote, RequestLogs

admin.site.register(InventoryRequest)
admin.site.register(InventoryQuote)
admin.site.register(RequestLogs)

