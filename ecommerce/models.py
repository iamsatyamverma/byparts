from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.db.models.signals import pre_delete
from django.dispatch.dispatcher import receiver
from django_mysql.models.fields.json import JSONField

from core.models import ModelBase, Model, CustomImageAttribute, Part, OrganizationStore, \
    CustomUserAttribute
from django.db.models import fields


#TODO: Need to change status english
class InventoryRequest(ModelBase):
    REQUEST_STATUS = [('OPEN', 'OPEN'), ('CANCELLED', 'CANCELLED'), ('CLOSED', 'CLOSED'),
                      ('DRAFTED', 'DRAFTED'), ('WITHDRAWN', 'WITHDRAWN'), ('COMPLETED', 'COMPLETED'),
                      ('EXPIRED', 'EXPIRED')]
    TRANSMISSION_TYPES = [('GEAR', 'GEAR'), ('AUTOMATIC', 'AUTOMATIC')]
    request_id = fields.CharField(max_length=127, unique=True)
    model = models.ForeignKey(Model, on_delete=models.CASCADE)
    registration = fields.CharField(max_length=127)
    model_detail = fields.TextField(null=True, blank=True)
    year_of_manufacture = fields.CharField(max_length=127)
    chassis_number = fields.CharField(max_length=255, null=True, blank=True)
    # lens_number = fields.CharField(max_length=255, null=True, blank=True)
    transmission = models.CharField(max_length=127, choices=TRANSMISSION_TYPES, default='GEAR')
    color = fields.CharField(max_length=127, null=True, blank=True)
    engine_size = fields.FloatField(null=True, blank=True)
    expiry = models.DateTimeField()
    delivery_location = fields.TextField()
    status = fields.CharField(max_length=127, choices=REQUEST_STATUS, default='OPEN')
    actions = GenericRelation(CustomUserAttribute, on_delete=models.CASCADE)
    gallery = GenericRelation(CustomImageAttribute)
    requested_inventory = JSONField(default=list)

class InventoryQuote(ModelBase):
    QUOTE_STATUS = [('OPEN', 'OPEN'), ('CANCELLED', 'CANCELLED'),
                    ('CLOSED', 'CLOSED'), ('DRAFTED', 'DRAFTED'),
                    ('ACCEPTED', 'ACCEPTED'), ('EXPIRED', 'EXPIRED'),
                    ('REPORTED', 'REPORTED'), ('REQUOTE', 'REQUOTE'),
                    ('REJECTED', 'REJECTED'), ('WITHDRAWN', 'WITHDRAWN')]
    request = models.ForeignKey(InventoryRequest, on_delete=models.CASCADE)
    quote_id = fields.CharField(max_length=127, unique=True)
    delivery_location = fields.TextField()
    estimated_delivery_time = fields.IntegerField()
    expiry = models.DateTimeField()
    status = fields.CharField(max_length=127, choices=QUOTE_STATUS, default='OPEN')
    actions = GenericRelation(CustomUserAttribute, on_delete=models.CASCADE)
    quoted_inventory = JSONField(default=list)



class RequestLogs(ModelBase):
    inventory = models.ForeignKey(InventoryRequest, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING, null=True, blank=True)
    heading = fields.CharField(max_length=255)
    comments = fields.TextField()
