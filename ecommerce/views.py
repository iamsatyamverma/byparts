from rest_framework import viewsets, status
from rest_framework.exceptions import PermissionDenied
from rest_framework.response import Response

from core.models import CustomImageAttribute
from core.resources import PART_IMAGE_CONTENT_TYPE
from ecommerce.models import InventoryRequest, InventoryQuote
from ecommerce.serializers import InventoryRequestSerializer, InventoryQuoteSerializer


class InventoryRequestView(viewsets.ModelViewSet):
    queryset = InventoryRequest.objects.all()
    serializer_class = InventoryRequestSerializer

    def update(self, request, *args, **kwargs):
        inventory_request_object = self.get_object()
        all_images = inventory_request_object.gallery.all()
        all_images.update(is_active=False)

        response = super(InventoryRequestView, self).update(request, *args, **kwargs)

        all_images.exclude(is_active=True).delete()
        return response

class InventoryQuoteView(viewsets.ModelViewSet):
    queryset = InventoryQuote.objects.all()
    serializer_class = InventoryQuoteSerializer


class InventoryRequestActionView(viewsets.ViewSet):
    # permission_classes = (permissions.IsAuthenticated,)

    def get_request_form(self, inventory_request_id):
        try:
            request_form = InventoryRequest.objects.get(id=inventory_request_id)
        except InventoryRequest.DoesNotExist:
            raise PermissionDenied({'details':"Can't find Request for id {}".format(inventory_request_id)})
        return request_form

    '''
    Dispatch the view according to the Url called. eg. /close, etc..
    '''
    def dispatch_action(self, request, pk, method, *args, **kwargs):
        actor = self.request.user
        inventory_request_form = self.get_request_form(pk)
        data = request.data
        return getattr(self, method)(inventory_request_form, actor, data)


    def get_success_headers(self, data):
        try:
            return {'Location': data.pk}
        except (TypeError, KeyError):
            return {}

    def cancel(self, inventory_request_form, actor, data):
        inventory_request_form.status = InventoryRequest.REQUEST_STATUS[1][0]
        inventory_request_form.save()
        response = inventory_request_form
        headers = self.get_success_headers(response)
        return Response(response.pk, status=status.HTTP_201_CREATED, headers=headers)

    def close(self, inventory_request_form, actor, data):
        inventory_request_form.status = InventoryRequest.REQUEST_STATUS[2][0]
        inventory_request_form.save()
        response = inventory_request_form
        headers = self.get_success_headers(response)
        return Response(response.pk, status=status.HTTP_201_CREATED, headers=headers)

    def withdraw(self, inventory_request_form, actor, data):
        inventory_request_form.status = InventoryRequest.REQUEST_STATUS[4][0]
        inventory_request_form.save()
        response = inventory_request_form
        headers = self.get_success_headers(response)
        return Response(response.pk, status=status.HTTP_201_CREATED, headers=headers)

    def complete(self, inventory_request_form, actor, data):
        inventory_request_form.status = InventoryRequest.REQUEST_STATUS[5][0]
        inventory_request_form.save()
        response = inventory_request_form
        headers = self.get_success_headers(response)
        return Response(response.pk, status=status.HTTP_201_CREATED, headers=headers)


class InventoryQuoteActionView(viewsets.ViewSet):
    # permission_classes = (permissions.IsAuthenticated,)

    def get_quote_form(self, inventory_quote_id):
        try:
            quote_form = InventoryQuote.objects.get(id=inventory_quote_id)
        except InventoryRequest.DoesNotExist:
            raise PermissionDenied({'details':"Can't find Quote for id {}".format(inventory_quote_id)})
        return quote_form

    '''
    Dispatch the view according to the Url called. eg. /close, etc..
    '''
    def dispatch_action(self, request, pk, method, *args, **kwargs):
        actor = self.request.user
        inventory_quote_form = self.get_quote_form(pk)
        data = request.data
        return getattr(self, method)(inventory_quote_form, actor, data)


    def get_success_headers(self, data):
        try:
            return {'Location': data.pk}
        except (TypeError, KeyError):
            return {}

    def cancel(self, inventory_quote_form, actor, data):
        inventory_quote_form.status = InventoryQuote.QUOTE_STATUS[1][0]
        inventory_quote_form.save()
        response = inventory_quote_form
        headers = self.get_success_headers(response)
        return Response(response.pk, status=status.HTTP_201_CREATED, headers=headers)

    def close(self, inventory_quote_form, actor, data):
        inventory_quote_form.status = InventoryQuote.QUOTE_STATUS[2][0]
        inventory_quote_form.save()
        response = inventory_quote_form
        headers = self.get_success_headers(response)
        return Response(response.pk, status=status.HTTP_201_CREATED, headers=headers)

    def accept(self, inventory_quote_form, actor, data):
        inventory_quote_form.status = InventoryQuote.QUOTE_STATUS[4][0]
        inventory_quote_form.save()
        response = inventory_quote_form
        headers = self.get_success_headers(response)
        return Response(response.pk, status=status.HTTP_201_CREATED, headers=headers)

    def report(self, inventory_quote_form, actor, data):
        inventory_quote_form.status = InventoryQuote.QUOTE_STATUS[6][0]
        inventory_quote_form.save()
        response = inventory_quote_form
        headers = self.get_success_headers(response)
        return Response(response.pk, status=status.HTTP_201_CREATED, headers=headers)

    def requote(self, inventory_quote_form, actor, data):
        inventory_quote_form.status = InventoryQuote.QUOTE_STATUS[7][0]
        inventory_quote_form.save()
        response = inventory_quote_form
        headers = self.get_success_headers(response)
        return Response(response.pk, status=status.HTTP_201_CREATED, headers=headers)

    def reject(self, inventory_quote_form, actor, data):
        inventory_quote_form.status = InventoryQuote.QUOTE_STATUS[8][0]
        inventory_quote_form.save()
        response = inventory_quote_form
        headers = self.get_success_headers(response)
        return Response(response.pk, status=status.HTTP_201_CREATED, headers=headers)

    def withdraw(self, inventory_quote_form, actor, data):
        inventory_quote_form.status = InventoryQuote.QUOTE_STATUS[9][0]
        inventory_quote_form.save()
        response = inventory_quote_form
        headers = self.get_success_headers(response)
        return Response(response.pk, status=status.HTTP_201_CREATED, headers=headers)
