from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from core.models import CustomAttributeType, Part, CustomImageAttribute, TemporaryFiles
from core.resources import PART_IMAGE_CONTENT_TYPE, create_image_attribute
from ecommerce.models import InventoryRequest, InventoryQuote


#TODO: Need to add validation that no other part except requested will be allowed unless suggested.
class QuotedPartSerializer(serializers.Serializer):
    PART_CONDITIONS = (('REFURBISHED', 'REFURBISHED'), ('SECOND_HAND', 'SECOND_HAND'), ('BRAND_NEW', 'BRAND_NEW'))
    part = serializers.IntegerField(allow_null=False, min_value=1)
    condition = serializers.ChoiceField(choices=PART_CONDITIONS)
    location = serializers.CharField()
    quantity = serializers.IntegerField(allow_null=False)
    comments = serializers.StringRelatedField(allow_null=True)
    is_suggested = serializers.BooleanField(default=False)

    def validate(self, attrs):
        validated_data = super(QuotedPartSerializer, self).validate(attrs)

        if (not Part.objects.filter(id=validated_data.get('part', -1)).exists()):
            raise ValidationError({'details':'Part not found'})

        return validated_data

#TODO: Add part path matches with id validation
class PartImageSerializer(serializers.Serializer):
    id = serializers.IntegerField(allow_null=False, min_value=1)
    path = serializers.CharField(allow_null=False)

    def to_internal_value(self, data):
        temp_id = data.get('temp_id', None)

        if temp_id:
            if not TemporaryFiles.objects.filter(id=temp_id).exists():
                raise ValidationError({'details':'Uploaded file does not exist'})
            return {'temp_id': temp_id}
        else:
            part_images = CustomImageAttribute.objects.filter(id=data['id'])

            if not part_images.exists():
                return ValidationError({'details': 'image with id not found'})

        return super(PartImageSerializer, self).to_internal_value(data)

    def to_representation(self, instance):
        return instance

    def create(self, validated_data):
        inventory_request = validated_data.pop('inventory_request')

        if validated_data.get('temp_id', None):
            part_image_attr_type = CustomAttributeType.objects.get(name='PART_IMAGE')

            path = 'request/{}'.format(inventory_request.id)
            content_type = PART_IMAGE_CONTENT_TYPE

            gallery_image = create_image_attribute(validated_data['temp_id'], inventory_request.id, part_image_attr_type, path, content_type)

            return gallery_image

        old_image = CustomImageAttribute.objects.filter(id=validated_data['id'])
        old_image.update(is_active=True)

        return validated_data


class RequestedPartSerializer(serializers.Serializer):
    part = serializers.IntegerField(allow_null=False, min_value=1)
    quantity = serializers.IntegerField(allow_null=False)
    comments = serializers.StringRelatedField(allow_null=True)
    gallery = PartImageSerializer(required=False)

    def validate(self, attrs):
        validated_data = super(RequestedPartSerializer, self).validate(attrs)

        if (not Part.objects.filter(id=validated_data.get('part', -1)).exists()):
            raise ValidationError({'details':'Part not found'})

        return validated_data

    def create(self, validated_data):
        part_image_serializer = PartImageSerializer(data=validated_data['gallery'])
        part_image_serializer.is_valid()
        part_image_serializer.save(**{'inventory_request':validated_data['inventory_request']})

        validated_data['gallery'] = part_image_serializer.data

        return validated_data


class InventoryRequestSerializer(serializers.ModelSerializer):
    requested_inventory = RequestedPartSerializer(many=True)

    class Meta:
        model = InventoryRequest
        fields = ('id', 'model', 'registration', 'model_detail',\
                  'year_of_manufacture', 'chassis_number',\
                  'request_id', 'transmission', 'color', 'engine_size',\
                  'expiry', 'delivery_location', 'status', 'requested_inventory')

    def create(self, validated_data):
        #TODO Add validation here if not happening automatically
        requested_inventory_list = validated_data.get('requested_inventory')

        if (requested_inventory_list == []):
            raise ValidationError({"details":"Inventory not found"})

        requested_inventory_serializer = RequestedPartSerializer(data=requested_inventory_list, many=True)
        requested_inventory_serializer.is_valid(raise_exception=True)

        inventory_request = InventoryRequest.objects.create(**validated_data)

        requested_inventory_serializer.save(**{'inventory_request':inventory_request})

        inventory_request.requested_inventory = requested_inventory_serializer.data
        inventory_request.save()

        return inventory_request

    def update(self, instance, validated_data):
        requested_inventory_list = validated_data.get('requested_inventory', [])

        if (requested_inventory_list == []):
            raise ValidationError({"details": "Inventory not found"})

        requested_inventory_serializer = RequestedPartSerializer(data=requested_inventory_list, many=True)
        requested_inventory_serializer.is_valid(raise_exception=True)
        requested_inventory = validated_data.pop('requested_inventory')

        instance = super(InventoryRequestSerializer, self).update(instance=instance, validated_data=validated_data)

        requested_inventory_serializer.save(**{'inventory_request': instance})
        instance.requested_inventory = requested_inventory_serializer.data
        instance.save()

        return instance


class InventoryQuoteSerializer(serializers.ModelSerializer):
    quoted_inventory = QuotedPartSerializer(many=True)

    def create(self, validated_data):
        #TODO Add validation here if not happening automatically
        quoted_inventory_list = validated_data.get('quoted_inventory')

        if (quoted_inventory_list == []):
            raise ValidationError({"details": "Inventory not found"})

        quoted_inventory_serializer = QuotedPartSerializer(data=quoted_inventory_list, many=True)
        quoted_inventory_serializer.is_valid(raise_exception=True)

        inventory_quote = InventoryQuote.objects.create(**validated_data)

        return inventory_quote

    def update(self, instance, validated_data):
        #TODO Add validation here if not happening automatically
        quoted_inventory_list = validated_data.get('quoted_inventory', [])

        if (quoted_inventory_list == []):
            raise ValidationError({"details": "Inventory not found"})

        quoted_inventory_serializer = QuotedPartSerializer(data=quoted_inventory_list, many=True)
        quoted_inventory_serializer.is_valid(raise_exception=True)
        quoted_inventory = validated_data.pop('quoted_inventory')

        instance = super(InventoryQuoteSerializer, self).update(instance=instance, validated_data=validated_data)
        instance.quoted_inventory = quoted_inventory
        instance.save()

        return instance

    class Meta:
        model = InventoryQuote
        fields = ('id', 'request', 'quote_id', 'delivery_location', 'estimated_delivery_time',\
                  'expiry', 'status', 'quoted_inventory')
