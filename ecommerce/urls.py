from django.conf.urls import url, include
from rest_framework import routers
from ecommerce.views import InventoryRequestView, InventoryQuoteView, InventoryRequestActionView, \
    InventoryQuoteActionView

router = routers.DefaultRouter()
router.register(r'request', InventoryRequestView)
router.register(r'quote', InventoryQuoteView)

inventory_request_action_dispatcher = InventoryRequestActionView.as_view({'post': 'dispatch_action'})
inventory_quote_action_dispatcher = InventoryQuoteActionView.as_view({'post': 'dispatch_action'})


urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'request/(?P<pk>[0-9]+)/(?P<method>\w+)/', inventory_request_action_dispatcher),
    url(r'quote/(?P<pk>[0-9]+)/(?P<method>\w+)/', inventory_quote_action_dispatcher),
]
